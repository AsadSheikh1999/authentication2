//
//  LocalAuthentication.swift
//  LocalAuthenticationDemo
//
//  Created by mohammad.sheikh on 10/04/22.
//

import LocalAuthentication

struct LocalAuthentication {
    static var isFaceIdAvail: Bool {
        let context: LAContext = LAContext()
        let evaluate = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        return (context.biometryType == .faceID) && evaluate
    }
    
    fileprivate var defaultReasonMessage: String {
        return LocalAuthentication.isFaceIdAvail ? kFaceIdAuthenticationReason : kTouchIdAuthenticationReason
    }
    
}
